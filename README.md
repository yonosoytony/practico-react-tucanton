# Practico-React-tuCanton

This is a test project that has the purpose of 
demonstrate Frontend capabilities. 

The instructions to install this test are:
1.-Clone this repository  
2.-navigate into the project folder  
3.- npm install  
4.- npm run 

this code has also been build and deployed 
[here](https://antoniosalinasolivares.github.io/practico-react-tucanton/)
